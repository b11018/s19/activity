// alert("s19 activity")

/*
    1. Declare 3 variables without initialization called username,password and role.
*/

let username
let password
let role

/*
    
    2. Create a login function which is able to prompt the user to provide their username, password and role.
        -use prompt() and update the username,password and role global variable with the prompt() returned values.
        -add an if statement to check if the the username is an empty string or null or if the password is an empty string or null or if the role is an empty string or null.
            -if it is, show an alert to inform the user that their input should not be empty.
        -add an else statement. Inside the else statement add a switch to check the user's role add 3 cases and a default:
                -if the user's role is admin, show an alert with the following message:
                    "Welcome back to the class portal, admin!"
                -if the user's role is teacher, show an alert with the following message:
                    "Thank you for logging in, teacher!"
                -if the user's role is a rookie, show an alert with the following message:
                    "Welcome to the class portal, student!"
                -if the user's role does not fall under any of the cases, as a default, show a message:
                    "Role out of range."
*/
function loginFunc(){
	username = prompt("Username:").toLowerCase();
	password = prompt("Password:").toLowerCase();
	role = prompt("Role:").toLowerCase();
}

loginFunc();
console.log(username, password, role)

if(username == "" || password == "" || role == ""){
	alert("Invalid entry in one or more fields.")
} else {
	switch(role){
		case 'admin' :
		alert("Welcome back to the class portal, admin!");
		break;

		case 'teacher' :
		alert("Thank you for logging in, teacher!");
		break;

		case 'rookie' :
		alert("Welcome to the class portal, student!");
		break;

		default:
		console.log("Role out of range.");
		break;
	};
};



// }
/*
    3. Create a function which is able to receive 4 numbers as arguments calculate its average and log a message for  the user about their letter equivalent in the console.
        -add parameters appropriate to describe the arguments.
        -create a new function scoped variable called avg.
        -calculate the average of the 4 number inputs and store it in the variable avg.
        -add an if statement to check if the value of avg is less than or equal to 74.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show avg>. The letter equivalent is F"
        -add an else if statement to check if the value of avg is greater than or equal to 75 and if avg is less than or equal to 79.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show avg>. The letter equivalent is D"
        -add an else if statement to check if the value of avg is greater than or equal to 80 and if avg is less than or equal to 84.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show avg>. The letter equivalent is C"
        -add an else if statement to check if the value of avg is greater than or equal to 85 and if avg is less than or equal to 89.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show avg>. The letter equivalent is B"
        -add an else if statement to check if the value of avg is greater than or equal to 90 and if avg is less than or equal to 95.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show avg>. The letter equivalent is A"
        -add an else if statement to check if the value of avg is greater than 96.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show avg>. The letter equivalent is A+"

        Invoke and add a number as argument using the browser console.

        Stretch goal:
            -Add an if statement to check the role of the user.
                -if the role of the user is currently "teacher" or "admin" or undefined or null, show an alert:
                "<role>! You are not allowed to access this feature!".
                -else, continue to evaluate the letter equivalent of the student's average.

*/
if(role === 'teacher' || 'admin' || '' || null){
	alert(role + "! You are not allowed to access this feature!")
} else {
	console('You shall pass!');
}


function checkAverage(grade1, grade2, grade3, grade4){
	avg = (grade1 + grade2 + grade3 + grade4) / 4;
	if(avg <= 74){
		console.log("Hello, student, your average is " + avg + ". The letter equivalent is F")
	} else if(avg >= 75 && avg <= 79){
		console.log("Hello, student, your average is " + avg + ". The letter equivalent is D")
	} else if(avg >= 80 && avg <= 84){
		console.log("Hello, student, your average is " + avg + ". The letter equivalent is C")
	} else if(avg >= 85 && avg <= 89){
		console.log("Hello, student, your average is " + avg + ". The letter equivalent is B")
	} else if(avg >= 90 && avg <= 95){
		console.log("Hello, student, your average is " + avg + ". The letter equivalent is A")
	} else if(avg >= 96){
		console.log("Hello, student, your average is " + avg + ". The letter equivalent is A+")
	}
}

console.log("checkAverage(70, 70, 72, 71)");
checkAverage(70, 70, 72, 71);
console.log("checkAverage(76,76,77,79)");
checkAverage(76,76,77,79);
console.log("checkAverage(81, 83, 84, 85)");
checkAverage(81, 83, 84, 85);
console.log("checkAverage(87, 88, 88, 89)");
checkAverage(87, 88, 88, 89);
console.log("checkAverage(91, 90, 92, 90)");
checkAverage(91, 90, 92, 90);
console.log("checkAverage(96, 95, 97, 97)");
checkAverage(96, 95, 97, 97);


